﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Characters.Battle;

namespace Assets.Scripts.Interfaces
{
    public interface ICharacterBattle_State
    {
        void Start();
        void Update();
        void SwitchState(BattleState newState);
    }
}
