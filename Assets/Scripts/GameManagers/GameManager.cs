﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Spawner;
using Assets.Scripts.Characters;

namespace Assets.Scripts.GameManagers
{
    public class GameManager : MonoBehaviour
    {

        IGameStateManager gameState; 

        bool isTickEnabled = true;

        public IGameStateManager GameState
        {
            get { return gameState; }
        }

        public bool IsTickEnabled
        {
            get { return isTickEnabled; }
            set { isTickEnabled = value; }
        }

        void Awake()
        {
            
            if (GameState == null)
            {
                SwitchState(new Overworld_State(this));
            }
            DontDestroyOnLoad(transform.gameObject);
            if(File.Exists(Application.persistentDataPath + "/spawnerInfo.dat"))
            {
                File.Delete(Application.persistentDataPath + "/spawnerInfo.dat");
            }
        }

        void Start()
        {
            ToggleMouseLock();
            
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ToggleMouseLock();
            }
//            print(GameState);
            gameState.Update();
         //   print(isTickEnabled);
        }

        void ToggleMouseLock()
        {
            Cursor.visible = !Cursor.visible;
            if(Cursor.lockState != CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        public void SwitchState(IGameStateManager newState)
        {
            StartCoroutine(StateSwitching(newState));
            
        }

        IEnumerator StateSwitching(IGameStateManager newState)
        {
            while(GameState != newState)
            {
                gameState = newState;

                yield return null;
            }

            yield return new WaitForFixedUpdate();

            gameState.Start();
        }

        public void SaveEncounterPosition(Vector3 position)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/spawnerInfo.dat");

            SpawnerPosition spawnerPos = new SpawnerPosition();
            spawnerPos.x = position.x;
            spawnerPos.y = position.y;
            spawnerPos.z = position.z;

            bf.Serialize(file, spawnerPos);
        }

        public void LoadEncounterPosition(OverworldSpawner spawner)
        {
            if(File.Exists(Application.persistentDataPath + "/spawnerInfo.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/spawnerInfo.dat", FileMode.Open);
                SpawnerPosition spawnerPos = (SpawnerPosition)bf.Deserialize(file);
                file.Close();

                spawner.position = new Vector3(spawnerPos.x, spawnerPos.y, spawnerPos.z);
            }
            else
            {
                spawner.position = spawner.gameObject.transform.position;
            }
        }

    }
}
