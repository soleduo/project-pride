﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Characters.Battle;

namespace Assets.Scripts.GameManagers
{
    public class UIFunctionLibrary : MonoBehaviour
    {

        public GameManager manager
        {
            get; set;
        }

        public CharacterBattle_Base activeCharacter
        {
            get; set;
        }
        
        public void Clicked_Command(int index)
        {
        //    Debug.Log("Active Character is " + activeCharacter + ".");
        //    Debug.Log("Command clicked is: " + activeCharacter.CommandList[0]+ ".");
            activeCharacter.CommandList[index].CommandEnter();
        }

        public void Clicked_Target(int index)
        {
            activeCharacter.SetTarget(index);
        }



        public void NewGame()
        {

        }
    }
}
