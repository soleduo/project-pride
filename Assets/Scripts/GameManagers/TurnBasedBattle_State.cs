﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Characters.Battle;

namespace Assets.Scripts.GameManagers
{
    public class TurnBasedBattle_State : IGameStateManager
    {

        public List<CharacterBattle_Base> characters = new List<CharacterBattle_Base> { };
        public List<CharacterBattle_Base> allies = new List<CharacterBattle_Base> { };
        public List<CharacterBattle_Base> enemies = new List<CharacterBattle_Base> { };
        public List<Slider> AP_Sliders = new List<Slider> { };
        public List<Slider> Cast_Sliders = new List<Slider> { };
        public List<Slider> HP_Sliders = new List<Slider> { };
        public List<Slider> TP_Sliders = new List<Slider> { };

        GameManager managerRef;

        public TurnBasedBattle_State(GameManager manager)
        {
            managerRef = manager;
            if (SceneManager.GetActiveScene().name != "BattleScene")
                SceneManager.LoadScene("BattleScene");
        }

        bool battleStart;

        public void Start()
        {
            //            Debug.Log(this + " Start.");
            GetAvailableCharacters();
            GetAllyCharacter();
            GetEnemyCharacter();
            GetAPSlider();
            GetCastSlider();
            GetHPSlider();
            battleStart = true;
        }

        public void Update()
        {
            if(battleStart == true && enemies.Count == 0)
            {
                EndBattle();
            }

            ConnectAPSlider();
            ConnectCastSlider();
            ConnectHPSlider();
        }

        void EndBattle()
        {
            managerRef.SwitchState(new Overworld_State(managerRef));
        }

        void GetAvailableCharacters()
        {

            CharacterBattle_Base[] charactersRef = CharacterBattle_Base.FindObjectsOfType<CharacterBattle_Base>();
            //            Debug.Log(charactersRef.Length);
            foreach (CharacterBattle_Base character in charactersRef)
            {
                characters.Add(character);
                //                Debug.Log("Adding " + character + "to Character List.");
            }
        }

        void GetAllyCharacter()
        {
            foreach (CharacterBattle_Base character in characters)
            {
                if (character.tag == "Ally" && !allies.Contains(character))
                {
                    allies.Add(character);
                    // Debug.Log("Ally List " + allies.Count + ": " + character);
                }
            }
        }

        void GetEnemyCharacter()
        {
            foreach (CharacterBattle_Base character in characters)
            {
                if (character.tag == "Enemy" && !enemies.Contains(character))
                {
                    enemies.Add(character);
                    //   Debug.Log("Enemy List " + enemies.Count + ": " + character);
                }
            }
        }

        void GetAPSlider()
        {
            Slider[] slidersRef = Slider.FindObjectsOfType<Slider>();
            foreach (Slider slider in slidersRef)
            {
                if (slider.tag == "AP_Slider" && !AP_Sliders.Contains(slider))
                {
                    //    Debug.Log("Add AP Slider" + slider);
                    AP_Sliders.Add(slider);
                    AP_Sliders.Sort();
                }
            }
        }

        void GetCastSlider()
        {
            Slider[] slidersRef = Slider.FindObjectsOfType<Slider>();
            foreach (Slider slider in slidersRef)
            {
                if (slider.tag == "Cast_Slider" && !Cast_Sliders.Contains(slider))
                {
                    Cast_Sliders.Add(slider);
                    Cast_Sliders.Sort();
                }
            }
        }

        void ConnectAPSlider()
        {
            for (int i = 0; i < allies.Count; i++)
            {
                //   Debug.Log("Connect AP Slider");
                CharacterBattle_State stateMachineRef = allies[i].stateMachine as CharacterBattle_State;

                AP_Sliders[i].value = stateMachineRef.CurrentAP;
            }
        }

        void ConnectCastSlider()
        {
            for (int i = 0; i < allies.Count; i++)
            {
                CharacterBattle_State stateMachineRef = allies[i].stateMachine as CharacterBattle_State;

                if (stateMachineRef.ActiveState == BattleState.CAST)
                    Cast_Sliders[i].value = stateMachineRef.GetRemainingCastTime();
                else
                {
                    if (Cast_Sliders[i].value <= 1)
                    {
                        Cast_Sliders[i].value += Time.deltaTime;
                        Mathf.Clamp(Cast_Sliders[i].value, 0, 1);
                    }
                }

            }
        }

        void ConnectHPSlider()
        {
            for (int i = 0; i < allies.Count; i++)
            {
                //   Debug.Log("Connect AP Slider");
                HP_Sliders[i].value = (allies[i].currentHP / allies[i].CharacterClass.SecondaryStats.hitPoint);
                HP_Sliders[i].GetComponentInChildren<Text>().text = allies[i].currentHP.ToString();
            }
        }

        void GetHPSlider()
        {
            Slider[] slidersRef = Slider.FindObjectsOfType<Slider>();
            foreach (Slider slider in slidersRef)
            {
                if (slider.tag == "HP_Slider" && !HP_Sliders.Contains(slider))
                {
                    //    Debug.Log("Add AP Slider" + slider);
                    HP_Sliders.Add(slider);
                    HP_Sliders.Sort();
                }
            }
        }
        void GetTPSlider()
        {
            Slider[] slidersRef = Slider.FindObjectsOfType<Slider>();
            foreach (Slider slider in slidersRef)
            {
                if (slider.tag == "TP_Slider" && !TP_Sliders.Contains(slider))
                {
                    //    Debug.Log("Add AP Slider" + slider);
                    TP_Sliders.Add(slider);
                    TP_Sliders.Sort();
                }
            }
        }

        public void CharacterDead(CharacterBattle_Base character)
        { if (character.tag == "Enemy")
            {
                enemies.Remove(character);
                character.DestroyObject();
            }
        }
    }
}
