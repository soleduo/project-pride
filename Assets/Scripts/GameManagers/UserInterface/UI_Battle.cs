﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Characters.Battle;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Assets.Scripts.GameManagers.UserInterface
{
    public class UI_Battle : IBattleUI
    {
        public Canvas activeCanvas { get; set; }

        public EventSystem eventSystemRef { get; set; }

        UIFunctionLibrary uiFunctionLibraryRef;

        protected Button[] buttonList;

        CharacterBattle_Base activeCharacter;
        public CharacterBattle_Base ActiveCharacter
        {
            get { return activeCharacter; }
            set { activeCharacter = value; }
        }

        public void ShowUI()
        {
            activeCanvas.enabled = true;
        }

        public void HideUI()
        {
            activeCanvas.enabled = false;


            foreach (Button button in buttonList)
                SetButtonNavigation(button, Navigation.Mode.None);
                

        }

        protected void FindEventSystem()
        {
            eventSystemRef = EventSystem.FindObjectOfType<EventSystem>();
        }

        protected void FindUIFunctionLibrary()
        {
            uiFunctionLibraryRef = UIFunctionLibrary.FindObjectOfType<UIFunctionLibrary>();
            uiFunctionLibraryRef.activeCharacter = activeCharacter;
        }

        protected void FindCanvas(string canvasName)
        {
            Canvas[] availableCanvas = Canvas.FindObjectsOfType<Canvas>();
            for (int i = 0; i < availableCanvas.Length; i++)
            {
                if (availableCanvas[i].name == canvasName)
                {
                    activeCanvas = availableCanvas[i];
                    ShowUI();
                }

            }
        }

        protected void SetSelectedToButton1(Button[] buttonListRef)
        {
           // Debug.Log("Count");
            eventSystemRef.SetSelectedGameObject(buttonListRef[0].gameObject);

           
        }

        protected void SetButtonNavigation(Button button, Navigation.Mode navMode)
        {
            Navigation navRef = new Navigation();
            navRef.mode = navMode;
          
            button.navigation = navRef;
        }
    }
}
