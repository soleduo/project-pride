﻿
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using Assets.Scripts.Characters.Battle;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.GameManagers.UserInterface
{
    public class UI_BattleCommand : UI_Battle
    {

        public UI_BattleCommand(CharacterBattle_Base characterRef)
        {
            ActiveCharacter = characterRef;
            FindEventSystem();
            FindUIFunctionLibrary();
            FindCanvas("BattleCommand");
            GetCanvasButtons();
        }
        
        
        
        void GetCanvasButtons()
        {
            buttonList = activeCanvas.GetComponentsInChildren<Button>();
            for(int i = 0; i < Mathf.Min(buttonList.Length, ActiveCharacter.CommandList.Count); i++)
            {
                Text text = buttonList[i].GetComponentInChildren<Text>();
                text.text = ActiveCharacter.CommandList[i].Name;
                SetButtonNavigation(buttonList[i], Navigation.Mode.Automatic);
            //    Debug.Log(i + " " + text.text);
            }
            SetSelectedToButton1(buttonList);
        }
        


        
        
    }
}
