﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GameManagers.UserInterface
{
    public class UI_Text
    {
        Canvas activeCanvas;

        public UI_Text(string canvasName, string text)
        {
            FindCanvas(canvasName);
            SetText(text);
        }

        void ShowUI()
        {
            activeCanvas.enabled = true;
        }

        public void HideUI()
        {
            activeCanvas.enabled = false;
        }

        public void SetText(string textRef)
        {
            activeCanvas.GetComponentInChildren<Text>().text = textRef;
        }

        protected void FindCanvas(string canvasName)
        {
            Canvas[] availableCanvas = Canvas.FindObjectsOfType<Canvas>();
            for (int i = 0; i < availableCanvas.Length; i++)
            {
                if (availableCanvas[i].name == canvasName)
                {
                    activeCanvas = availableCanvas[i];
                    ShowUI();
                }

            }
        }
    }
}
