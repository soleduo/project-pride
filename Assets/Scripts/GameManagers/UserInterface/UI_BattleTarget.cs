﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts.Characters.Battle;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.GameManagers.UserInterface
{
    public class UI_BattleTarget : UI_Battle
    {

        List<CharacterBattle_Base> enemyList = new List<CharacterBattle_Base> { };

        public UI_BattleTarget(CharacterBattle_Base characterRef)
        {
            ActiveCharacter = characterRef;
            FindEventSystem();
            FindUIFunctionLibrary();
            FindCanvas("BattleTarget");
            AddEnemyList();
            GetCanvasButtons();
        }



        void AddEnemyList()
        {
            CharacterBattle_Base[] enemies = CharacterBattle_Base.FindObjectsOfType<CharacterBattle_Base>();
            foreach(CharacterBattle_Base enemy in enemies)
            {
                if(enemy.tag == "Enemy" && !enemyList.Contains(enemy))
                {
                    enemyList.Add(enemy);
                }
            }
        }

        void GetCanvasButtons()
        {
            buttonList = activeCanvas.GetComponentsInChildren<Button>();
            for (int i = 0; i < enemyList.Count; i++)
            {
                //Debug.Log(buttonList[i].transform.position);
                buttonList[i].transform.position = new Vector3(enemyList[i].transform.position.x, .2f, enemyList[i].transform.position.z)  ;
               // Debug.Log(buttonList[i].transform.position + ", " + enemyList[i].transform.position);
                SetButtonNavigation(buttonList[i], Navigation.Mode.Automatic);
            }
            SetSelectedToButton1(buttonList);
        }

    }
}
