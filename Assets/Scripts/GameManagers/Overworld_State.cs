﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Interfaces;
using UnityEngine.SceneManagement;
using Assets.Scripts.Characters.Battle;
using Assets.Scripts.Spawner;

namespace Assets.Scripts.GameManagers
{

    public enum OverworldState
    {
        IDLE,
        TEXT
    }

    public class Overworld_State : IGameStateManager
    {

        GameManager managerRef;


        public OverworldState overworldState;

        public Overworld_State(GameManager manager)
        {
            managerRef = manager;
            if (SceneManager.GetActiveScene().name != "OverworldScene")
                SceneManager.LoadScene("OverworldScene");
        }

        public void Start()
        {
            SwitchState(OverworldState.IDLE);
        }

        public void Update()
        {

        }

        

        public void SwitchState(OverworldState newState)
        {
            overworldState = newState;
        }

        public void OnEncounter(Vector3 encounterPosition)
        {
            managerRef.SaveEncounterPosition(encounterPosition);
            managerRef.SwitchState(new TurnBasedBattle_State(managerRef));
        }

        public void EndInteract()
        {
            SwitchState(OverworldState.IDLE);
        }
    }
}
