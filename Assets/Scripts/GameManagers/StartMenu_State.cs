﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Characters.Battle;


namespace Assets.Scripts.GameManagers
{
    public class StartMenu_State : IGameStateManager
    {

        GameManager managerRef;

        public StartMenu_State(GameManager manager)
        {
            managerRef = manager;
        }

        public void Start()
        {

        }

        public void Update()
        {

        }

        public void OnNewGame()
        {
            managerRef.SwitchState(new Overworld_State(managerRef));
        }
    }
}
