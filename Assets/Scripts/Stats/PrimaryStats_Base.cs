﻿using UnityEngine;

namespace Assets.Scripts.Stats
{
    public struct PrimaryStats_Base
    {
        public int level;

        float strength;
        float agility;
        float intelligence; 
        public float[] primaryStats { get; set; }

        public PrimaryStats_Base(int level,float STR, float AGI, float INT)
        {
            this.level = level;

            strength = STR;
            agility = AGI;
            intelligence = INT;
            

            primaryStats = new float[] { strength, agility, intelligence };
        }

        public SecondaryStats_Base LevelUp(StatGrowth growth, SecondaryStats_Base secondaryStats)
        {
            level += 1;
 //           Debug.Log("Level is: " + level);

            primaryStats[0] += growth.strength;
            primaryStats[1] += growth.agility;
            primaryStats[2] += growth.intelligence;

            secondaryStats.LevelUp(growth);
            return secondaryStats;
        }
    }
}
