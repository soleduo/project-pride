﻿using UnityEngine;

namespace Assets.Scripts.Stats
{
    public struct SecondaryStats_Base
    {
        public float hitPoint;
        public float techPoint;
        public float meleeAttack;
        public float rangedAttack;
        public float armor;
        public float magicAttack;
        public float magicDefense;

        public SecondaryStats_Base(float[] baseStats, PrimaryStats_Base primaryStats)
        {
            hitPoint = baseStats[0];
            meleeAttack = baseStats[2];

            rangedAttack = baseStats[3];
            armor = baseStats[4];

            techPoint = baseStats[1];
            magicAttack = baseStats[5];
            magicDefense = baseStats[6];

            ApplyPrimaryStats(primaryStats);
        }

        public int RecoverHPtoMax(int currentHP)
        {
            currentHP = Mathf.FloorToInt(hitPoint);
            return currentHP;
        }

        public void ApplyPrimaryStats(PrimaryStats_Base basePrimaryStats)
        {
            hitPoint += basePrimaryStats.primaryStats[0] * 19;
            meleeAttack += basePrimaryStats.primaryStats[0];

            rangedAttack += basePrimaryStats.primaryStats[1];
            armor += basePrimaryStats.primaryStats[1] * 0.14f;

            techPoint += basePrimaryStats.primaryStats[2] * 13;
            magicAttack += basePrimaryStats.primaryStats[2];
            magicDefense += basePrimaryStats.primaryStats[2] * 0.14f;
        }

        public void LevelUp(StatGrowth growth)
        {
            hitPoint += growth.strength * 19;
            meleeAttack += growth.strength;

            rangedAttack += growth.agility;
            armor += growth.agility * 0.14f;

            techPoint += growth.intelligence * 13;
            magicAttack += growth.intelligence;
            magicDefense += growth.intelligence * 0.14f;
        }
    }
}
