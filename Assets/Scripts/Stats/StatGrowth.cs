﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Stats
{
    public struct StatGrowth
    {
        public float strength;
        public float agility;
        public float intelligence;

        
        public StatGrowth(float STR, float AGI, float INT)
        {
            strength = STR;
            agility = AGI;
            intelligence = INT;
        }
    }
}
