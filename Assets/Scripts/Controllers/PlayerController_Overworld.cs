﻿using UnityEngine;
using Assets.Scripts.GameManagers;
using Assets.Scripts.Characters;


namespace Assets.Scripts.Controllers
{
    public class PlayerController_Overworld
    {
        GameManager managerRef;
        Overworld_State gameStateRef;

        CharacterOverworld_Player playerRef;

        Camera activeCamera;
        Vector3 camForward;
        Vector3 camRight;

        bool tryInteract;

        
        public bool TryInteract { get { return tryInteract; } }

        public PlayerController_Overworld(GameManager manager, CharacterOverworld_Player character)
        {
            managerRef = manager;
            playerRef = character;

            activeCamera = Camera.main;
            
            if (managerRef.GameState.GetType() == typeof(Overworld_State))
            {
                gameStateRef = managerRef.GameState as Overworld_State;
            }
            
        }

        public void Update()
        {
            GetCameraDirection();

            switch (gameStateRef.overworldState)
            {
                case (OverworldState.IDLE):
                    OnIdle();
                    break;
                case (OverworldState.TEXT):
                    OnText();
                    break;
            }
        }

        void OnIdle()
        {
            MovementControl();
            tryInteract = InteractControl();
        }

        void OnText()
        {
            tryInteract = InteractControl();
            
        }

        void MovementControl()
        {
            Vector3 moveRight = Input.GetAxis("Horizontal") * camRight;
            Vector3 moveForward = Input.GetAxis("Vertical") * camForward;

            Vector3 moveDir = moveRight + moveForward;

 //           Debug.Log(moveDir);

            playerRef.Movement(moveDir);
        }

        void GetCameraDirection()
        {
            camForward = new Vector3(activeCamera.transform.forward.x, 0, activeCamera.transform.forward.z);
            camRight = new Vector3(activeCamera.transform.right.x, 0, activeCamera.transform.right.z);
        }

        bool InteractControl()
        {
            if (Input.GetButtonDown("Submit"))
                return true;
            else
                return false;
        }
    }
}
