﻿using Assets.Scripts.Characters.Battle;
using Assets.Scripts.GameManagers;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Commands;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Controllers
{
    public class AIController_Battle
    {
        CharacterBattle_Base characterRef;

        GameManager managerRef;

        public AIController_Battle(CharacterBattle_Base characterRef, GameManager managerRef)
        {
            this.characterRef = characterRef;
            this.managerRef = managerRef;
        }

        int GetRandomNumber(int maxOption)
        {
            int i = Random.Range(0, maxOption);
            return i;
        }

        public void SelectAction()
        {
            List<Command_Base> commandList = characterRef.CommandList;
            characterRef.QueuedCommand = commandList[GetRandomNumber(commandList.Count)];
        }

        public void SelectTarget()
        {
            TurnBasedBattle_State battleManager = managerRef.GameState as TurnBasedBattle_State;
            List<CharacterBattle_Base> availableTarget = battleManager.allies;
            characterRef.Target = availableTarget[GetRandomNumber(availableTarget.Count)];
        }
    }
}
