﻿using Assets.Scripts.Characters.Battle;

namespace Assets.Scripts.Commands
{
    public class Command_Base
    {
        private string name; //Command Name
        private string description; //Command Description

        private float commandValue; //Damage/Healing Value

        private float costAP;
        private float castTime;
        private float attackRange;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public float CommandValue
        {
            get { return commandValue; }
            set { commandValue = value; }
        }

        public float APCost
        {
            get { return costAP; }
            set { costAP = value; }
        }

        public float CastTime
        {
            get { return castTime; }
            set { castTime = value; }
        }

        public float AttackRange
        {
            get { return attackRange; }
            set { attackRange = value; }
        }


        public virtual void CommandEnter()
        {

        }

        public virtual void CommandExecute()
        {
            
        }

        public void CommandEnd(CharacterBattle_Base charaRef)
        {
            charaRef.StateMachineRef.CommandEnd();
        }
    }
}
