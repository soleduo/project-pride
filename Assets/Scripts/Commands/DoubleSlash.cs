﻿using Assets.Scripts.Characters.Battle;
using UnityEngine;

namespace Assets.Scripts.Commands
{
    public class DoubleSlash : Command_Base
    {
        CharacterBattle_Base characterRef;

        public DoubleSlash(CharacterBattle_Base characterRef)
        {
            Name = "Double Slash";
            Description = "Slash the enemy twice.";

            CommandValue = 40f;

            APCost = 0.8f;
            CastTime = .225f;
            AttackRange = 1.2f;

            this.characterRef = characterRef;
        }

        public override void CommandEnter()
        {
            characterRef.QueuedCommand = this;
            //    Debug.Log("Queue: " + Name);
        }

        public override void CommandExecute()
        {

//            Debug.Log(characterRef.name + " use " + Name + "! " + characterRef.Target.name + " took " + CommandValue + " damage.");
        }
    }
}
