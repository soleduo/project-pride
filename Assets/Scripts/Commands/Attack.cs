﻿using Assets.Scripts.Characters.Battle;
using UnityEngine;
namespace Assets.Scripts.Commands
{
    public class Attack : Command_Base
    {

        CharacterBattle_Base characterRef;
        
        public Attack(CharacterBattle_Base characterRef)
        {
            Name = "Attack";
            Description = "Attack with Equipped Weapon";
            
            CommandValue = characterRef.CharacterClass.SecondaryStats.meleeAttack * 0.75f;

            APCost = 0.30f;
            CastTime = 0.1f;
            AttackRange = 1.2f;

            this.characterRef = characterRef;
        }

        public override void CommandEnter()
        {
            characterRef.QueuedCommand = this;
        //    Debug.Log("Queue: " + Name);
        }


        public override void CommandExecute()
        {
            if (characterRef.Target != null)
            {
                characterRef.Target.ApplyDamage(CommandValue);
                characterRef.Target.OnHit();
            }
 //           Debug.Log(characterRef.name + " use " + Name + "! " + characterRef.Target.name + " took " + CommandValue + " damage.");
        }

        
    }
}
