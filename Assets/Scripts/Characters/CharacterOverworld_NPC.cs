﻿using UnityEngine;

namespace Assets.Scripts.Characters
{
    public enum NPC_Type
    {
        FRIENDLY,
        HOSTILE
    }

    class CharacterOverworld_NPC : CharacterOverworld_Base
    {
        public NPC_Type NPCType;
        void Start()
        {
            if(tag == "Enemy")
            {
                NPCType = NPC_Type.HOSTILE;
            }
            else
            {
                NPCType = NPC_Type.FRIENDLY;
            }
        }
        
    }
}
