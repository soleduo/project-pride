﻿using UnityEngine;
using Assets.Scripts.GameManagers;
using Assets.Scripts.GameManagers.UserInterface;

namespace Assets.Scripts.Characters
{
    public class Interactibles_Base : MonoBehaviour
    {
        GameManager managerRef;

        Overworld_State gameStateRef;

        UI_Text textBoxRef;

        public bool IsActive { get; set; }

        void Start()
        {
            managerRef = FindObjectOfType<GameManager>();
            gameStateRef = managerRef.GameState as Overworld_State;
        }

        public void OnInteract()
        {
            gameStateRef.SwitchState(OverworldState.TEXT);
            textBoxRef = new UI_Text("TextBoxCanvas", "Interaction with " + name);
            IsActive = true;
        }

        public void EndInteract()
        {
            textBoxRef.HideUI();
            gameStateRef.EndInteract();
            IsActive = false;
            
        }
    }
}
