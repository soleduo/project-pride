﻿using Assets.Scripts.Characters.Battle;
using Assets.Scripts.Stats;

namespace Assets.Scripts.Characters.Classes
{
    public class CharacterClass_Bandit : CharacterClass_Base
    {

        public CharacterClass_Bandit(CharacterBattle_Base character, int level)
        {
            characterRef = character;

            this.level = level;
            name = "Bandit";

            PrimaryStats = new PrimaryStats_Base(1, 17, 34, 14);
            StatGrowth = new StatGrowth(1.6f, 2.2f, 1.3f);
            SecondaryStats = new SecondaryStats_Base(new float[] { 0, 0, 8, 23, 0, 15, 25}, PrimaryStats);
        }

    }
}
