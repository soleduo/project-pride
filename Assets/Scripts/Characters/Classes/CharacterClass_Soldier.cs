﻿using Assets.Scripts.Characters.Battle;
using Assets.Scripts.Stats;

namespace Assets.Scripts.Characters.Classes
{
    public class CharacterClass_Soldier : CharacterClass_Base
    {
        

        public CharacterClass_Soldier(CharacterBattle_Base character)
        {
            characterRef = character;

            level = 2;
            name = "Soldier";

            PrimaryStats = new PrimaryStats_Base(1, 23, 21, 16);
            StatGrowth = new StatGrowth(2.7f, 2f, 1.3f);
            SecondaryStats = new SecondaryStats_Base(new float[] { 180f, 0f, 84, 21, 2, 5, 25}, PrimaryStats);
        }
    }
}
