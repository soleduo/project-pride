﻿using System;
using UnityEngine;
using Assets.Scripts.Stats;
using Assets.Scripts.Characters.Battle;

namespace Assets.Scripts.Characters.Classes
{
    public class CharacterClass_Base
    {
        public int level;
        public string name { get; set; }

        public CharacterBattle_Base characterRef;
        PrimaryStats_Base primaryStats;
        StatGrowth statGrowth;
        SecondaryStats_Base secondaryStats;

        public PrimaryStats_Base PrimaryStats {
            get { return primaryStats; }
            set { primaryStats = value; }
        }

        public SecondaryStats_Base SecondaryStats
        {
            get { return secondaryStats; }
            set { secondaryStats = value; }
        }

        public StatGrowth StatGrowth
        {
            get { return statGrowth; }
            set { statGrowth = value; }
        }

        public void Update()
        {
            if(level >= primaryStats.level)
            {
                LevelUp();
            }
        }

        public void LevelUp()
        {
            secondaryStats = primaryStats.LevelUp(statGrowth, secondaryStats);
            
            characterRef.currentHP = secondaryStats.RecoverHPtoMax(characterRef.currentHP);
        }
    }
}
