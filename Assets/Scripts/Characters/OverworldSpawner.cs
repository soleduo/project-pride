﻿using UnityEngine;
using System;
using System.Collections;
using Assets.Scripts.Characters;
using Assets.Scripts.GameManagers;

namespace Assets.Scripts.Spawner
{


    public class OverworldSpawner : MonoBehaviour {

        GameManager managerRef;
        public UnityEngine.Object spawnedCharacter;
        [HideInInspector]
        public Vector3 position;

	    // Use this for initialization
	    void Start () {
            managerRef = FindObjectOfType<GameManager>();
            if (managerRef != null)
            {
                managerRef.LoadEncounterPosition(this);
            }
            SpawnCharacter();
	    }
	
	    // Update is called once per frame
	    void Update () {
	
	    }

        void SpawnCharacter()
        {
            Debug.Log("Player spawned at: " + position + transform.position);
            Instantiate(spawnedCharacter, transform.localPosition = position, transform.rotation);
        }
    }

    [Serializable]
    class SpawnerPosition
    {
        public float x;
        public float y;
        public float z;
    }

}
