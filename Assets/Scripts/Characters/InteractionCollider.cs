﻿using UnityEngine;
using Assets.Scripts.Controllers;

namespace Assets.Scripts.Characters
{
    public class InteractionCollider : MonoBehaviour
    {

        PlayerController_Overworld playerControllerRef;

        void Start()
        {
            playerControllerRef = FindObjectOfType<CharacterOverworld_Player>().PlayerController;
        }

        void OnTriggerStay(Collider collider)
        {
      //      print("Can interact with: " + collider.gameObject.name);
            if (collider.tag == "Interactibles" && playerControllerRef.TryInteract)
            {
                Interactibles_Base interactiblesRef = collider.gameObject.GetComponent<Interactibles_Base>();

                if (!interactiblesRef.IsActive)
                    interactiblesRef.OnInteract();
                else
                    interactiblesRef.EndInteract();

             }
                    
                else
                    print("Interaction script not found.");
            
        }
    }
}
