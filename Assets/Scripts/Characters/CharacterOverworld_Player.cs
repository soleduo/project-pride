﻿using UnityEngine;
using Assets.Scripts.Controllers;
using Assets.Scripts.GameManagers;

namespace Assets.Scripts.Characters
{
    [RequireComponent(typeof(CharacterController))]
    public class CharacterOverworld_Player : CharacterOverworld_Base
    {
        GameManager managerRef;

        CharacterController controllerRef;
        PlayerController_Overworld playerControllerRef;

        public PlayerController_Overworld PlayerController { get { return playerControllerRef; } }

        void Awake()
        {
            
            //    print("Awake. " + activeCamera + controllerRef);
        }

        void Start()
        {
            managerRef = FindObjectOfType<GameManager>() as GameManager;
  //          print(managerRef);


            controllerRef = GetComponent<CharacterController>();
            playerControllerRef = new PlayerController_Overworld(managerRef, this);
        }

        void Update()
        {
            playerControllerRef.Update();
        }

        public override void Movement(Vector3 moveDir)
        {

            // print(moveDir);

            moveDir.Normalize();
            moveDir *= moveSpeed;

            if(moveDir != new Vector3(0,0,0))
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDir), 15 * Time.deltaTime);

            controllerRef.SimpleMove(moveDir);
        }

        


    }
}
