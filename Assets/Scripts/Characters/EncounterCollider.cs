﻿using UnityEngine;
using Assets.Scripts.GameManagers;

namespace Assets.Scripts.Characters
{
    public class EncounterCollider : MonoBehaviour
    {
        GameManager managerRef;
        Overworld_State gameStateRef;

        void Awake()
        {
            managerRef = FindObjectOfType<GameManager>() as GameManager;
            if(managerRef.GameState.GetType() == typeof(Overworld_State))
                gameStateRef = managerRef.GameState as Overworld_State;
        }

        void OnTriggerEnter(Collider collider)
        {
            if (collider.GetComponent<CharacterOverworld_NPC>() != null)
            {
                if (collider.GetComponent<CharacterOverworld_NPC>().NPCType == NPC_Type.HOSTILE)
                {
///                    print("EncounterStart");
                    gameStateRef.OnEncounter(gameObject.transform.position);
                }
            }
        }
    }
}
