﻿using UnityEngine;

namespace Assets.Scripts.Characters
{

    public enum InteractionType
    {
        TALK,
        SHOP,
        INN,
        QUEST
    }

    public class CharacterOverworld_Base : MonoBehaviour
    {
        public float moveSpeed;

        public virtual void Movement(Vector3 moveDir)
        {

        }
    }
}
