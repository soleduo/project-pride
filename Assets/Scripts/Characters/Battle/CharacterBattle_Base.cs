﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Commands;
using Assets.Scripts.GameManagers;
using UnityEngine.UI;
using Assets.Scripts.Characters.Classes;
using Assets.Scripts.Controllers;


namespace Assets.Scripts.Characters.Battle
{
    public class CharacterBattle_Base : MonoBehaviour
    {

        public ICharacterBattle_State stateMachine { get; set; }
        List<Command_Base> commandList;
        CharacterBattle_Movement movementController;
        CharacterBattle_State stateMachineRef;
        GameManager manager;
        Command_Base queuedCommand;
        Animation animate;
        CharacterBattle_Base target;
        protected  CharacterClass_Base cClass;

        public int currentHP;
        public int currentTP;

        public CharacterClass_Base CharacterClass
        {
            get { return cClass; }
        }

        public AIController_Battle AIController { get; set; }

        public List<Command_Base> CommandList
        {
            get { return commandList; }
            set { commandList = value; }
        }

        public Command_Base QueuedCommand
        {
            get { return queuedCommand; }
            set { queuedCommand = value; }
        }

        public CharacterBattle_Base Target
        {
            get { return target; }
            set { target = value; }
        }

        public CharacterBattle_Movement MovementController
        {
            get { return movementController; }
        }

        public CharacterBattle_State StateMachineRef
        {
            get { return stateMachineRef; }
        }

        protected virtual void Awake()
        {
            if (movementController == null)
            {
                movementController = new CharacterBattle_Movement(this);
            }
            if (manager == null)
            {
                manager = GameManager.FindObjectOfType<GameManager>();
            }
            if(stateMachine == null)
            {
                stateMachine = new CharacterBattle_State(this, manager);
            }
            
            
            stateMachineRef = stateMachine as CharacterBattle_State; 
        }

        void Start()
        {
            if(gameObject.tag != "Ally")
            {
                AIController = new AIController_Battle(this, manager);
            }

            currentHP = Mathf.FloorToInt(cClass.SecondaryStats.hitPoint);
            currentTP = Mathf.FloorToInt(cClass.SecondaryStats.techPoint);
        //    animate = gameObject.GetComponent<Animation>();
        }

        void Update()
        {
            if(currentHP <= 0)
            {
                Dead();
            }

            cClass.Update();
            

            stateMachine.Update();
            
        }

        void Dead()
        {
            TurnBasedBattle_State stateRef = manager.GameState as TurnBasedBattle_State;
            StateMachineRef.SwitchState(BattleState.DEAD);
            stateRef.CharacterDead(this);
        }

        public void DestroyObject()
        {
            
            Destroy(gameObject);
        }

        public void SetTarget(int index)
        {
            if(this.manager.GameState.GetType() == typeof(TurnBasedBattle_State))
            {
                TurnBasedBattle_State manager = this.manager.GameState as TurnBasedBattle_State;
                Target = manager.enemies[index];
            //    Debug.Log("Set target: " + target);
            }
        }

        float timer;

        public IEnumerator AttackSequence()
        {
            
            yield return new WaitForSeconds(1.2f);

            while (stateMachineRef.CurrentAP >= QueuedCommand.APCost)
            {
                if (!IsAttacked())
                {
                    if (!movementController.HasArrived)
                    {
                        movementController.SetDestination(movementController.SetTarget(Target.gameObject));
                        movementController.ActionMovement();
                    }
                    else
                    {
                        QueuedCommand.CommandExecute();
                        stateMachineRef.CurrentAP -= QueuedCommand.APCost;
                        yield return new WaitForSeconds(1.2f);
                    }
                }
                else
                {
                   
                    yield return new WaitForSeconds(1.2f);

                    onHit = false;

                }
            }
            
            yield return new WaitForFixedUpdate();
            QueuedCommand.CommandEnd(this);
        }

        public void ApplyDamage(float damage)
        {
            currentHP -= Mathf.FloorToInt(damage);
        }

        bool onHit;

        bool IsAttacked()
        {
            return onHit;
        }

        public void OnHit()
        {
            onHit = true;
        }
       
    }
}
