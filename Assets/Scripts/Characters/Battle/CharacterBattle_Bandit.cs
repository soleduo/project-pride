﻿using Assets.Scripts.Characters.Classes;
using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Commands;

namespace Assets.Scripts.Characters.Battle
{
    public class CharacterBattle_Bandit : CharacterBattle_Base
    {
        protected override void Awake()
        {
            base.Awake();
            name = "Bandit";
            int i = Random.Range(1, 3);
            cClass = new CharacterClass_Bandit(this, i);
            CommandList = new List<Command_Base> { new Attack(this)};
        }
    }
}
