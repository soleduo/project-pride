﻿using Assets.Scripts.Commands;
using Assets.Scripts.GameManagers;
using Assets.Scripts.Characters.Classes;
using Assets.Scripts.Controllers;
using System.Collections.Generic;

namespace Assets.Scripts.Characters.Battle
{
    public class CharacterBattle_Shou : CharacterBattle_Base
    {
        protected override void Awake()
        {
            base.Awake();
            name = "Shou";
            cClass = new CharacterClass_Soldier(this);
            CommandList = new List<Command_Base> { new Attack(this)};
        }
    }
}
