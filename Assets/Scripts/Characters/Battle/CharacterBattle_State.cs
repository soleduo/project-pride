﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Interfaces;
using UnityEngine.UI;
using Assets.Scripts.GameManagers;
using Assets.Scripts.GameManagers.UserInterface;
using Assets.Scripts.Commands;

namespace Assets.Scripts.Characters.Battle
{
    
    public enum BattleState
    {
        IDLE,
        COMMAND,
        CAST,
        ACTION,
        DEAD
    }

    public class CharacterBattle_State : ICharacterBattle_State
    {

        BattleState activeState;
        public BattleState ActiveState { get { return activeState; } }

        GameManager managerRef;
        CharacterBattle_Base characterRef;

        float currentAP;
        

        float castTimer;
       
        IBattleUI uiRef;

        bool isAttacking;

        CharacterBattle_Movement movementControllerRef;

        //GETTER/SETTER

        public CharacterBattle_State (CharacterBattle_Base ownerRef, GameManager managerRef)
        {
            characterRef = ownerRef;
            this.managerRef = managerRef;
            movementControllerRef = characterRef.MovementController;
        }

        public float CurrentAP
        {
            get { return currentAP; }
            set { currentAP = value; }
        }

        public float CastTimer
        {
            get { return castTimer; }
        }

        public float GetRemainingCastTime()
        {
            float remainingTime = (characterRef.QueuedCommand.CastTime - castTimer) / characterRef.QueuedCommand.CastTime;
            return remainingTime;
        }

        public bool IsAttacking
        {
            get { return isAttacking; }
            set { isAttacking = value; }
        }

        // Use this for initialization
        public void Start()
        {

        }

        // Update is called once per frame
        public void Update()
        {
            //Debug.Log("Has arrived: " + movementControllerRef.HasArrived);
             //   Debug.Log(activeState);
                switch (activeState)
                {
                    case BattleState.IDLE:
                        OnIdle();
                        break;
                    case BattleState.COMMAND:
                        OnCommand();
                        break;
                    case BattleState.CAST:
                        OnCast();
                        break;
                    case BattleState.ACTION:
                        OnAction();
                        break;
                    case BattleState.DEAD:
                        OnDead();
                        break;
            }
        }

        public void SwitchState(BattleState newState)
        {
            activeState = newState;
        }


        //STATE UPDATE FUNCTION

        void OnIdle()
        {
            LookAt();

            if (currentAP >= 1)
            {
                SwitchState(BattleState.COMMAND); //If currentAP == 100, switch to Command State
            }
            else
            {
                if(managerRef.IsTickEnabled)
                currentAP += ((characterRef.CharacterClass.PrimaryStats.primaryStats[1]/99) * Time.deltaTime); //AP Tick
            }
        }

        void OnCommand()
        {


            LookAt();

            if (characterRef.gameObject.tag == "Ally")
            {

                if (characterRef.QueuedCommand == null)
                {
                    
                    ActivateCommandUI();
                }
                else
                {
                    if (characterRef.Target == null)
                    {
                        managerRef.IsTickEnabled = false;
                        ActivateTargettingUI();
                    }
                    else
                    {
                        uiRef.HideUI();
                        managerRef.IsTickEnabled = true;
                        SwitchState(BattleState.CAST);
                    }
                }
            }
            else
            {
                
                if (characterRef.QueuedCommand != null && characterRef.Target != null)
                {
                    SwitchState(BattleState.CAST);
                }
                else
                {
                    characterRef.AIController.SelectAction();
                    characterRef.AIController.SelectTarget();
                }
            }
        }



        void OnCast()
        {

            LookAt();
               
            if (castTimer >= characterRef.QueuedCommand.CastTime) //if castTimer >= abilityCastTime, switch to Action State
            {
                
                isAttacking = true;
                SwitchState(BattleState.ACTION);
            }
            if (managerRef.IsTickEnabled)
                castTimer += (0.1f * Time.deltaTime);
        }

        void OnAction()
        {
           
                
                if (movementControllerRef.HasArrived)
                {
                    //                Debug.Log("6");
                    if (isAttacking)
                    {
                        
                            characterRef.StartCoroutine(characterRef.AttackSequence());
                     
                            isAttacking = false;
                    }
                
                }
                else
                {
                    //                Debug.Log("1");
                    movementControllerRef.SetDestination(movementControllerRef.SetTarget(characterRef.Target.gameObject));
                    movementControllerRef.ActionMovement();
                }
          
             
        }

        void OnDead()
        {
            
        }
        //UI ACTIVATOR

        void ActivateCommandUI()
        {
            if (uiRef != null && uiRef.GetType() == typeof(UI_BattleCommand))
            {
                uiRef.ShowUI();    
            }
            else
            {
               // Debug.Log("Count");
                uiRef = new UI_BattleCommand(characterRef);
            }
                
        }

        void DestroyUI()
        {
            uiRef.HideUI();
        }

        void ActivateTargettingUI()
        {
            if (uiRef != null && uiRef.GetType() == typeof(UI_BattleTarget))
            {
                uiRef.ShowUI();
            }
            else
            {
                uiRef.HideUI();
                uiRef = new UI_BattleTarget(characterRef);
            }
                
        }


        // VARIABLE RESETTER

        void ResetAllInstances()
        {
            
            castTimer = 0;
            characterRef.QueuedCommand = null;
            characterRef.Target = null;

            movementControllerRef.HasArrived = false;

        }

        void LookAt()
        {
            if (characterRef.gameObject.tag == "Ally")
            {
                movementControllerRef.LookAt("Enemy");
            }
            else
            {
                movementControllerRef.LookAt("Ally");
            }
        }

        public void CommandEnd()
        {
            ResetAllInstances();
            movementControllerRef.FreeMovement();
            SwitchState(BattleState.IDLE);
        }
    }
}
