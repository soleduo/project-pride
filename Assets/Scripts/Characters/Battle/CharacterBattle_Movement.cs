﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.GameManagers;

namespace Assets.Scripts.Characters.Battle
{
    public class CharacterBattle_Movement
    {
        CharacterBattle_Base characterRef;
        NavMeshAgent agentRef;
        GameManager managerRef;

        Vector3 destination;

        bool hasArrived = false;
        bool haveDestination;

        public CharacterBattle_Movement(CharacterBattle_Base characterRef)
        {
            this.characterRef = characterRef;
            agentRef = characterRef.gameObject.GetComponent<NavMeshAgent>();
            GetGameManager();
        }


        public bool HasArrived
        {
            get { return hasArrived; }
            set { hasArrived = value; }
        }

        public NavMeshAgent AgentRef { get { return agentRef; } }

        public void LookAt(string tag)
        {

                MovementSwitch();
                if (agentRef.remainingDistance <= agentRef.stoppingDistance && agentRef.remainingDistance != 0)
                {
                    agentRef.ResetPath();

                    agentRef.gameObject.transform.LookAt(GameObject.FindGameObjectWithTag(tag).transform.position);
                }
            
        }

        public void SetDestination(Vector3 destinationRef)
        {
        //    Debug.Log("3");
            
            haveDestination = true;
            hasArrived = false;
            agentRef.SetDestination(destinationRef);
        }

        public Vector3 SetTarget(GameObject target)
        {
//            Debug.Log("2");
            //Debug.Log("Set Target to: " + target.name);
            Vector3 targetPos = target.transform.position;
            return targetPos;
        }

        public void ActionMovement()
        {
            MovementSwitch();
//            Debug.Log(agentRef.gameObject.name + " Action Movement");
            if (!hasArrived)
            {
//                Debug.Log(agentRef.remainingDistance);
                if (characterRef.StateMachineRef.CurrentAP <= 0)
                {
                    //Debug.Log("titit");
                    hasArrived = true;
                }
                else
                {
                    //                    Debug.Log("4" +" "+ Time.timeSinceLevelLoad);
                    
                    characterRef.StateMachineRef.CurrentAP -= (0.09f * Time.deltaTime);
                }
                if (agentRef.remainingDistance - agentRef.stoppingDistance <= 0 && agentRef.remainingDistance != 0f)
                {
                        haveDestination = false;
                        hasArrived = true;
                }
            }
        }

        float RandomFloat()
        {
            System.Random random = new System.Random();
            float f = random.Next(8, 12);
            f *= Mathf.Sign(random.Next(-1, 1));
            return f;
            
        }

        public void FreeMovement()
        {
            if (!haveDestination)
            {
                Vector3 destination = new Vector3(RandomFloat(), 0f, RandomFloat());
                agentRef.SetDestination(Vector3.Scale(destination, characterRef.gameObject.transform.forward * -1f) + characterRef.gameObject.transform.position);
                //Debug.Log("Free movement destination: " + agentRef.destination);
                
            }
        }

        void GetGameManager()
        {
            managerRef = GameManager.FindObjectOfType<GameManager>();
        }

        void MovementSwitch()
        {
          //  Debug.Log(managerRef.IsTickEnabled);
           if(managerRef.IsTickEnabled)
            {
                agentRef.Resume();
                
            }
            else
            {
            //    Debug.Log("Stopping " + agentRef.gameObject.name);
                agentRef.Stop();
            }
        }
    }
}
